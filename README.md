# itemdb

**Gestión de Recursos Empresariales**



**¿Cómo iniciar?**

```bash
_ROOT=${HOME}/Workspace \
_HOME=${_ROOT}/itemdb.git \
docker-composer -f docker/docker-compose.yml build applications

```



**Estructura**

Cuando se monta el container, se generan dos directorios de trabajo:

- /opt/itemdb
  - El directorio con todo lo relacionado al container
- /opt/webroot
  - Directorio raíz donde se montan los proyectos.



**Lenguajes**

- Backend
  - https://lumen.laravel.com/
- Frontend
  - https://quasar.dev/



