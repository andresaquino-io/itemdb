# (c) 2019, Andres Aquino <andres.aquino(at)gmail.com>
# This file is licensed under the BSD License version 3 or later.
# See the LICENSE file.

# apache setup file.
# See apache project for more information.

#
# Description
#     Directorio base donde se encuentran los archivos
#     de configuracion
# ServerName ${__HOST}:${__PORT}
ServerRoot ${__HOME}

#
# 
Listen ${__LISTEN}:${__PORT}


#
# Description
#     Nombre del archivo y ubicacion donde se aloja el
#     PID del proceso de apache
PidFile run/${__INSTANCE}.pid

# Modules
# Minimal required modules
Include conf.modules.d/00-base.cnf
Include conf.modules.d/00-systemd.cnf
Include conf.modules.d/00-mpm-prefork.cnf
Include conf.modules.d/15-php.cnf

# and setup
Include conf.d/custerrors.cnf
Include conf.d/php.cnf
Include conf.d/appZabbix.cnf
Include conf.d/appUtils.cnf

#
# Description
#     Numero en segundos que deberan transcurrir para recibir
Timeout 60

#
# Description
#     Permitir conexiones persistentes (mas de una solicitud por
#     conexion), entiendase por solicitud (request) una peticion
#     de una imagen, una hoja de estilo, un bloque de datos, etc..
KeepAlive On

#
# Description
#     Numero maximo de solicitudes a admitir durante una conexion
#     persistente. Si se pone a 0 las solicitudes son ilimitadas,
#     Se recomienda un valor muy alto, para un mayor desempeno.
MaxKeepAliveRequests 256

#
# Description
#     Tiempo maximo en segundos a esperar en una solicitud (request) para
#     un cliente en una misma conexion.
KeepAliveTimeout 5

#
# Descripcion
#     Usuario y grupo bajo el cual el proceso de apache va a ejecutarse
#     de igual manera sirve para limitar los archivos a los cuales apache
#     tiene permitido leer y proveer en cualquier solicitud de servicio.
#     See /etc/sysconfig/loadbalancer
User ${__USER}
Group ${__GROUP}

# > MAIN SERVER SETUP
#
# ServerAdmin: Your address, where problems with the server should be
# e-mailed.  This address appears on some server-generated pages, such
# as error documents.  e.g. admin@your-domain.com
ServerAdmin ${__ADMIN}

#
# UseCanonicalName: Determines how Apache constructs self-referencing
# URLs and the SERVER_NAME and SERVER_PORT variables.
UseCanonicalName Off

#
# Each directory to which Apache has access can be configured with respect
# to which services and features are allowed and/or disabled in that
# directory (and its subdirectories).
<Directory />
   AllowOverride None
   Require all denied
</Directory>

#
# DocumentRoot: The directory out of which you will serve your documents
DocumentRoot "${__WWW}"
<Directory "${__ROOT}">
    AllowOverride None
    Require all granted

</Directory>

<Directory "${__WWW}">
    Options Indexes FollowSymLinks
    AllowOverride None
    Require all granted

</Directory>

#
# Aliases: Add here as many aliases as you need (with no limit). The format is
# Alias fakename realname
# /usr/share/httpd/icons/small/sound2.png
Alias /icons/ /usr/share/httpd/icons/
<Directory /usr/share/httpd/icons/>
   Options Indexes MultiViews FollowSymlinks
   AllowOverride None
   Require all granted
</Directory>

#Alias /e/ ${WEBHOME}/resources/errorpages/
#<Directory ${WEBHOME}/resources/errorpages/>
#    Options Indexes MultiViews
#    AllowOverride None
#    Order allow,deny
#    Allow from all
#</Directory>

#
# DirectoryIndex: sets the file that Apache will serve if a directory
# is requested.
DirectoryIndex index.html index.html.var

#
# AccessFileName: The name of the file to look for in each directory
# for additional configuration directives.  See also the AllowOverride
# directive.
AccessFileName .htaccess

#
# The following lines prevent .htaccess and .htpasswd files from being
# viewed by Web clients.
<Files "^\.ht">
   Require all denied
</Files>

#
# TypesConfig describes where the mime.types file (or equivalent) is
# to be found.
TypesConfig /etc/mime.types

#
# HostnameLookups: Log the names of clients or just their IP addresses
# e.g., www.apache.org (on) or 204.62.129.132 (off).
HostnameLookups Off

#
# EnableMMAP: Control whether memory-mapping is used to deliver
# files (assuming that the underlying OS supports it).
EnableMMAP off

#
# EnableSendfile: Control whether the sendfile kernel support is
# used  to deliver files (assuming that the OS supports it).
EnableSendfile on

#
# ErrorLog: The location of the error log file.
# If you do not specify an ErrorLog directive within a <VirtualHost>
# container, error messages relating to that virtual host will be
# logged here.
ErrorLog "${__DBASE}/log/${__INSTANCE}.err"

#
# LogLevel: Control the number of messages logged to the error_log.
# Possible values include: debug, info, notice, warn, error, crit,
# alert, emerg.
LogLevel info

#
# The following directives define some format nicknames for use with
# a CustomLog directive (see below).
LogFormat "%h %>s %t \"%r\" %>s %b \"%{Referer}i\" \"%{User-Agent}i\"" combined
LogFormat "%h %l %u %t \"%r\" %>s %b" common
LogFormat "%{Referer}i -> %U" referer
LogFormat "%{User-agent}i" agent
LogFormat "%s\t%T\t%X\t%B\t%{%F %T}t\t%r" analyze
LogFormat "%t %r = %>s from: %h " personal

#
# The location and format of the access logfile (Common Logfile Format).
# If you do not define any access logfiles within a <VirtualHost>
# container, they will be logged here.  Contrariwise, if you *do*
# define per-<VirtualHost> access logfiles, transactions will be
# logged therein and *not* in this file.
CustomLog "${__DBASE}/log/${__INSTANCE}.acc" common

#
# If you would like to have agent and referer logfiles, uncomment the
# following directives.
# CustomLog "${__DBASE}/log/${__INSTANCE}.ref" referer
# CustomLog "${__DBASE}/log/${__INSTANCE}.agn" agent
# CustomLog "${__DBASE}/log/${__INSTANCE}.anl" analyze

#
# If you prefer a single logfile with access, agent, and referer information
# (Combined Logfile Format) you can use the following directive.
# CustomLog "${__DBASE}/log/${__INSTANCE}.log" personal

#
# ServerTokens
# This directive configures what you return as the Server HTTP response
# Header. The default is 'Full' which sends information about the OS-Type
# and compiled in modules.
# Set to one of:  Full | OS | Minor | Minimal | Major | Prod
ServerTokens Prod

#
# Optionally add a line containing the server version and virtual host
# name to server-generated pages (internal error documents, FTP directory
# listings, mod_status and mod_info output etc., but not CGI generated
# documents or custom error documents).
# Set to "EMail" to also include a mailto: link to the ServerAdmin.
# Set to one of:  On | Off | EMail
ServerSignature Off

#
# IndexOptions: Controls the appearance of server-generated directory
# listings.
IndexOptions FancyIndexing HTMLTable VersionSort

#
# AddIcon* directives tell the server which icon to show for different
# files or filename extensions.  These are only displayed for
# FancyIndexed directories.
AddIconByEncoding (CMP,/icons/compressed.gif) x-compress x-gzip

AddIconByType (TXT,/icons/text.gif) text/*
AddIconByType (IMG,/icons/image2.gif) image/*
AddIconByType (SND,/icons/sound2.gif) audio/*
AddIconByType (VID,/icons/movie.gif) video/*

AddIcon /icons/binary.gif .bin .exe
AddIcon /icons/binhex.gif .hqx
AddIcon /icons/tar.gif .tar
AddIcon /icons/world2.gif .wrl .wrl.gz .vrml .vrm .iv
AddIcon /icons/compressed.gif .Z .z .tgz .gz .zip
AddIcon /icons/a.gif .ps .ai .eps
AddIcon /icons/layout.gif .html .shtml .htm .pdf
AddIcon /icons/text.gif .txt
AddIcon /icons/c.gif .c
AddIcon /icons/p.gif .pl .py
AddIcon /icons/f.gif .for
AddIcon /icons/dvi.gif .dvi
AddIcon /icons/uuencoded.gif .uu
AddIcon /icons/script.gif .conf .sh .shar .csh .ksh .tcl .php .phps
AddIcon /icons/tex.gif .tex
AddIcon /icons/bomb.gif core
AddIcon /icons/back.gif ..
AddIcon /icons/hand.right.gif README
AddIcon /icons/folder.gif ^^DIRECTORY^^
AddIcon /icons/blank.gif ^^BLANKICON^^
DefaultIcon /icons/unknown.gif

#
# ReadmeName is the name of the README file the server will look for by
# default, and append to directory listings.
ReadmeName README.html

#
# HeaderName is the name of a file which should be prepended to
# directory indexes.
HeaderName HEADER.html

#
# IndexIgnore is a set of filenames which directory indexing should ignore
# and not include in the listing.  Shell-style wildcarding is permitted.
IndexIgnore .??* *~ *# HEADER* README* RCS CVS *,v *,t .git

#
# DefaultLanguage and AddLanguage allows you to specify the language of
# a document. You can then use content negotiation to give a browser a
# file in a language the user can understand.
AddLanguage en .en
AddLanguage es .es

#
# LanguagePriority allows you to give precedence to some languages
# in case of a tie during content negotiation.
LanguagePriority en es

#
# ForceLanguagePriority allows you to serve a result page rather than
# MULTIPLE CHOICES (Prefer) [in case of a tie] or NOT ACCEPTABLE (Fallback)
# [in case no accepted languages matched the available variants]
ForceLanguagePriority Prefer Fallback

#
# Commonly used filename extensions to character sets. You probably
# want to avoid clashes with the language extensions, unless you
# are good at carefully testing your setup after each change.
# See http://www.iana.org/assignments/character-sets for the
# official list of charset names and their respective RFCs.
AddCharset UTF-8       .utf8
AddCharset ISO-8859-1  .iso8859-1  .latin1

#
# The set below does not map to a specific (iso) standard
# but works on a fairly wide range of browsers. Note that
# capitalization actually matters (it should not, but it
# does for some browsers).
AddCharset utf-8       .utf8

#
# AddType allows you to add to or override the MIME configuration
# file mime.types for specific file types.
AddType application/x-compress .Z
AddType application/x-gzip .gz .tgz

#
# For type maps (negotiated resources):
# (This is enabled by default to allow the Apache "It Worked" page
#  to be distributed in multiple languages.)
AddHandler type-map var

#
# The following directives modify normal HTTP response behavior to
# handle known problems with browser implementations.
BrowserMatch "Mozilla/2" nokeepalive
BrowserMatch "MSIE 4\.0b2;" nokeepalive downgrade-1.0 force-response-1.0
BrowserMatch "RealPlayer 4\.0" force-response-1.0
BrowserMatch "Java/1\.0" force-response-1.0
BrowserMatch "JDK/1\.0" force-response-1.0

#
# The following directive disables redirects on non-GET requests for
# a directory that does not include the trailing slash.  This fixes a
# problem with Microsoft WebFolders which does not appropriately handle
# redirects for folders with DAV methods.
# Same deal with Apple's DAV filesystem and Gnome VFS support for DAV.
BrowserMatch "Microsoft Data Access Internet Publishing Provider" redirect-carefully
BrowserMatch "MS FrontPage" redirect-carefully
BrowserMatch "^WebDrive" redirect-carefully
BrowserMatch "^WebDAVFS/1.[0123]" redirect-carefully
BrowserMatch "^gnome-vfs" redirect-carefully
BrowserMatch "^XML Spy" redirect-carefully
BrowserMatch "^Dreamweaver-WebDAV-SCM1" redirect-carefully

