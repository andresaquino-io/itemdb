# (c) 2019, Andres Aquino <andres@aquino.page>
# This file is licensed under the BSD License version 3 or later.

FROM centos:latest
MAINTAINER Andres Aquino <andres@aquino.page>

# System
ENV TIMEZONE="America/Mexico_City"
ENV TZ=${TIMEZONE}
ENV LANG en_US.UTF-8
ENV LANGUAGE en_US:en
ENV LC_ALL en_US.UTF-8

# packages
ENV PACKAGES \
   bash ansible

# Update installation & required pkgs
RUN yum repolist && yum update -y
RUN yum install -y ${PACKAGES}
#RUN rm -rf /var/cache/yum/*

# Timezone
RUN ln -sf /usr/share/zoneinfo/${TIMEZONE} /etc/localtime \
   && echo "${TZ}" > /etc/timezone

# Workdir
WORKDIR /opt/itemdb

# ansible resources
COPY ansible/hosts /opt/itemdb/hosts
COPY ansible/playbook.yml /opt/itemdb/playbook.yml
COPY keystore/id_rsa.pub /opt/itemdb/id_rsa.pub

# packages required
COPY resources/packages /opt/itemdb/packages
COPY resources/setup /opt/itemdb/resources

# provisioning with ansible (as a local service)
RUN /usr/bin/ansible-playbook /opt/itemdb/playbook.yml -i hosts

# start!
# ENTRYPOINT ["bash /opt/itemdb/start.sh"]

