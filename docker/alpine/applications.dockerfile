# (c) 2020, Andrés Aquino <andres@aquino.page>
# This file is licensed under the BSD License version 3 or later.

FROM alpine:latest
MAINTAINER Andres Aquino <andres@aquino.page>

# System
ENV TIMEZONE="America/Mexico_City"
ENV TZ=${TIMEZONE}
ENV LANG en_US.UTF-8
ENV LANGUAGE en_US:en
ENV LC_ALL en_US.UTF-8

# Args
ARG USER_ID
ARG GROUP_ID

# Packages
ENV PACKAGES \
   shadow bash ansible python

# Update installation & required pkgs
RUN apk update && apk upgrade
RUN apk add ${PACKAGES}
#RUN rm -rf /var/cache/apk/*

# Timezone
RUN ln -sf /usr/share/zoneinfo/${TIMEZONE} /etc/localtime \
   && echo "${TZ}" > /etc/timezone

# Workdir
WORKDIR /opt/itemdb

# ansible resources
ADD ansible /opt/itemdb/ansible

# keystore && packages
COPY keystore/id_rsa.pub /opt/itemdb/id_rsa.pub
COPY config/packages/arte-latest.tgz /opt/itemdb/arte-latest.tgz

# provisioning with ansible (as a local service)
RUN /usr/bin/ansible-playbook /opt/itemdb/ansible/playbook.yml -i ansible/hosts

