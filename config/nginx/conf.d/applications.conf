# (c) 2019, Andrés Aquino <inbox@andresaquino.sh>
# This file is licensed under the BSD License version 3 or later.
# See the LICENSE file.
#

server {
   listen 8081

   server_name localhost.test;

   # Root
	root /opt/assurance/default;

   # Security
	include /etc/nginx/conf.d/security.conf;

   location / {
      try_files $uri $uri/ /index.html;
   }

   # app.localhost/itemdb
   location /itemdb {
      proxy_pass http://applications:9001;
      proxy_redirect off;

      proxy_http_version 1.1;
    	proxy_set_header Upgrade $http_upgrade;
    	proxy_set_header Connection "upgrade";

      proxy_set_header Host $http_host;
      proxy_set_header X-Real-IP $remote_addr;
      proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;

      proxy_read_timeout 1m;
      proxy_connect_timeout 1m;
   }

   # app.localhost/api
   location /api {
      proxy_pass http://applications:9002;
      proxy_redirect off;

      proxy_http_version 1.1;
    	proxy_set_header Upgrade $http_upgrade;
    	proxy_set_header Connection "upgrade";

      proxy_set_header Host $http_host;
      proxy_set_header X-Real-IP $remote_addr;
      proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;

      proxy_read_timeout 1m;
      proxy_connect_timeout 1m;
   }

   # favicon.ico
   location = /favicon.ico {
	   log_not_found off;
	   access_log off;
   }

   # robots.txt
   location = /robots.txt {
	   log_not_found off;
	   access_log off;
   }

   # assets, media
   location ~* \.(?:css(\.map)?|js(\.map)?)$ {
	   expires modified +1m;
	   access_log off;
   }

   # assets, media
   location ~* \.(?:jpe?g|png|gif|ico|cur|heic|webp|tiff?|mp3|m4a|aac|ogg|midi?|wav|mp4|mov|webm|mpe?g|avi|ogv|flv|wmv)$ {
	   expires modified +2h;
	   access_log off;
   }

   # svg, fonts
   location ~* \.(?:svgz?|ttf|ttc|otf|eot|woff2?)$ {
	   add_header Access-Control-Allow-Origin "*";
	   expires modified +12h;
	   access_log off;
   }

   location ~/\.ht {
	   deny all;
   }

   # . files
   location ~ /\.(?!well-known) {
	   deny all;
   }

   # gzip
   gzip on;
   gzip_vary on;
   gzip_proxied any;
   gzip_comp_level 6;
   gzip_types text/plain text/css text/xml application/json application/javascript application/rss+xml application/atom+xml image/svg+xml;

}
